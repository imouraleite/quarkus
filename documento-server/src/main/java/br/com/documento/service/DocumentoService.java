package br.com.documento.service;

import java.io.IOException;
import java.util.Date;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.RandomUtils;

import br.com.documento.dao.DocumentoBinDAO;
import br.com.documento.dao.DocumentoDAO;
import br.com.documento.dao.UsuarioDAO;
import br.com.documento.entity.Documento;
import br.com.documento.entity.DocumentoBin;
import br.com.documento.entity.Usuario;
import br.com.documento.multipart.data.Arquivo;

@ApplicationScoped
public class DocumentoService {

	@Inject
	EntityManager entityManager;
	@Inject
	private UsuarioDAO usuarioDAO;
	@Inject
	private DocumentoBinDAO documentoBinDAO;
	@Inject
	private DocumentoDAO documentoDAO;

	@Transactional
	public String incluirDocumento(Arquivo arquivo) {
		StringBuilder mensagem = new StringBuilder();
		try {
			criarDocumento(arquivo);
			mensagem.append("Documento incluido com sucesso");
		} catch (Exception e) {
			mensagem.append("Erro ao incluir documento: ").append(e.getCause());
		}

		return mensagem.toString();
	}

	private DocumentoBin criarDocumentoBin(Arquivo arquivo) throws IOException {
		DocumentoBin docBin = new DocumentoBin();
		docBin.setArquivo(IOUtils.toByteArray(arquivo.getArquivo().getInputStream()));
		documentoBinDAO.persist(docBin);
		return docBin;
	}

	private void criarDocumento(Arquivo arquivo) throws IOException {
		Documento documento = new Documento();
		documento.setNomeDocumento(arquivo.getNome());
		documento.setDescDocumento("Documento Upload");
		documento.setNumeroDocumento(RandomUtils.nextInt(0, 100));
		documento.setDocumentoBin(criarDocumentoBin(arquivo));
		documento.setDataHoraInclusao(new Date());
		documento.setDataHoraAlteracao(new Date());
		documento.setUsuarioInclusao(recuperarUsuario());
		documento.setUsuarioAlteracao(recuperarUsuario());

		documentoDAO.persist(documento);
	}

	private Usuario recuperarUsuario() {
		return usuarioDAO.findById(Usuario.class, 1L);
	}

}
