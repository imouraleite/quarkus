package br.com.documento.dao;

import javax.enterprise.context.ApplicationScoped;

import br.com.documento.entity.DocumentoBin;

@ApplicationScoped
public class DocumentoBinDAO extends GenericoDAO<DocumentoBin> {

}
