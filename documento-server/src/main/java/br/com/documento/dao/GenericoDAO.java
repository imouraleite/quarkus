package br.com.documento.dao;

import javax.inject.Inject;
import javax.persistence.EntityManager;

public class GenericoDAO<T> {

	@Inject
	EntityManager entityManager;

	public T findById(Class<T> entityClass, Object id) {
		return entityManager.find(entityClass, id);
	}

	public void persist(Object object) {
		entityManager.persist(object);
		entityManager.flush();
	}

	public void update(Object object) {
		entityManager.merge(object);
		entityManager.flush();
	}

	public void delete(Object object) {
		entityManager.remove(object);
		entityManager.flush();
	}
}
