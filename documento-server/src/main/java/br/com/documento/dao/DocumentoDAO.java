package br.com.documento.dao;

import javax.enterprise.context.ApplicationScoped;

import br.com.documento.entity.Documento;

@ApplicationScoped
public class DocumentoDAO extends GenericoDAO<Documento> {

}
