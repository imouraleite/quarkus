package br.com.documento.dao;

import javax.enterprise.context.ApplicationScoped;

import br.com.documento.entity.Usuario;

@ApplicationScoped
public class UsuarioDAO extends GenericoDAO<Usuario> {

}
