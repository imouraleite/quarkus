package br.com.documento.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "tb_documento")
public class Documento implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(generator = "DocumentoSequenceGenerator", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "DocumentoSequenceGenerator", sequenceName = "sq_documento", allocationSize = 1, initialValue = 1)
	@Column(name = "id_documento")
	private Long id;
	
	@Column(name = "nm_documento")
	private String nomeDocumento;
	
	@Column(name = "ds_documento")
	private String descDocumento;
	
	@Column(name = "nr_documento")
	private Integer numeroDocumento;
	
	@OneToOne
    @JoinColumn(name = "id_documento_bin", referencedColumnName = "id_documento_bin")
	private DocumentoBin documentoBin;
	
	@Column(name = "dt_inclusao_documento")
	private Date dataHoraInclusao;
	
	@Column(name = "dt_alteracao_documento")
	private Date dataHoraAlteracao;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_usuario_inclusao", referencedColumnName = "id_usuario")
	private Usuario usuarioInclusao;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_usuario_alteracao", referencedColumnName = "id_usuario")
	private Usuario usuarioAlteracao;
	
	public Documento() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNomeDocumento() {
		return nomeDocumento;
	}

	public void setNomeDocumento(String nomeDocumento) {
		this.nomeDocumento = nomeDocumento;
	}

	public String getDescDocumento() {
		return descDocumento;
	}

	public void setDescDocumento(String descDocumento) {
		this.descDocumento = descDocumento;
	}

	public Integer getNumeroDocumento() {
		return numeroDocumento;
	}

	public void setNumeroDocumento(Integer numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	public DocumentoBin getDocumentoBin() {
		return documentoBin;
	}

	public void setDocumentoBin(DocumentoBin documentoBin) {
		this.documentoBin = documentoBin;
	}

	public Date getDataHoraInclusao() {
		return dataHoraInclusao;
	}

	public void setDataHoraInclusao(Date dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}

	public Date getDataHoraAlteracao() {
		return dataHoraAlteracao;
	}

	public void setDataHoraAlteracao(Date dataHoraAlteracao) {
		this.dataHoraAlteracao = dataHoraAlteracao;
	}

	public Usuario getUsuarioInclusao() {
		return usuarioInclusao;
	}

	public void setUsuarioInclusao(Usuario usuarioInclusao) {
		this.usuarioInclusao = usuarioInclusao;
	}

	public Usuario getUsuarioAlteracao() {
		return usuarioAlteracao;
	}

	public void setUsuarioAlteracao(Usuario usuarioAlteracao) {
		this.usuarioAlteracao = usuarioAlteracao;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Documento other = (Documento) obj;
		return Objects.equals(id, other.id);
	}

	@Override
	public String toString() {
		return "Documento [id=" + id + ", nomeDocumento=" + nomeDocumento + ", descDocumento=" + descDocumento
				+ ", numeroDocumento=" + numeroDocumento + ", documentoBin=" + documentoBin + ", dataHoraInclusao="
				+ dataHoraInclusao + ", dataHoraAlteracao=" + dataHoraAlteracao + ", usuarioInclusao=" + usuarioInclusao
				+ ", usuarioAlteracao=" + usuarioAlteracao + "]";
	}

}
