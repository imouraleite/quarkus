package br.com.documento.entity;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "tb_documento_bin")
public class DocumentoBin {
	
	@Id
	@GeneratedValue(generator = "DocumentoBinSequenceGenerator", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "DocumentoBinSequenceGenerator", sequenceName = "sq_documento_bin", allocationSize = 1, initialValue = 1)
	@Column(name = "id_documento_bin")
	private Long id;
	
	@Column(name="ar_documento_bin",nullable=true)
	private byte[] arquivo;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public byte[] getArquivo() {
		return arquivo;
	}

	public void setArquivo(byte[] arquivo) {
		this.arquivo = arquivo;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DocumentoBin other = (DocumentoBin) obj;
		return Objects.equals(id, other.id);
	}

	@Override
	public String toString() {
		return "DocumentoBin [id=" + id + "]";
	}

}
