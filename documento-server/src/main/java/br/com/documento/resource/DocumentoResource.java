package br.com.documento.resource;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.jboss.resteasy.annotations.providers.multipart.XopWithMultipartRelated;
import org.jboss.resteasy.plugins.providers.multipart.MultipartConstants;

import br.com.documento.multipart.data.Arquivo;
import br.com.documento.service.DocumentoService;

@Path("/upload")
public class DocumentoResource {

	@Inject
	DocumentoService documentoService;

	@POST
	@Consumes(MultipartConstants.MULTIPART_RELATED)
	@Produces(MediaType.TEXT_PLAIN)
	public String uploadDocumento(@XopWithMultipartRelated Arquivo arquivo) throws Exception {
		return documentoService.incluirDocumento(arquivo);
	}
}
