# Projeto documento-server

Este projeto usa quarkus website: https://quarkus.io/ .

## Executando a aplicação em modo dev

use:
```shell script
./mvn compile quarkus:dev
```

## Empacotando e executando a aplicação

Empacote a aplicação usando:
```shell script
./mvn package
```
Este comando produz um jar `quarkus-run.jar` no diretório `target/quarkus-app/`.
Execute a aplicação com `java -jar target/quarkus-app/quarkus-run.jar`.

# ORM
## Convenção de Nomenclatura
Na convenção de nomenclatura para artefatos do banco de dados utiliza-se um prefixo concatenado com '_' com o nome do artefato, além disso os nomes devem estar escritos em letras minúsculas e não podem conter caracteres especiais. A tabela abaixo apresenta os prefixos utilizados para artefatos do banco.

### tabelas, views e sequences

| Descrição | Prefixo |
| --------- | ------- |
|  Table    |   tb    |
|  Views    |   vs    |
| Sequence  |   sq    |

A convenção para os nomes de colunas de acordo com o domínio do dado, disponível na tabela abaixo.

|   Domínio     | Prefixo |
| ------------- | ------- |
| Identificador |   id    |
|    Código     |   cd    |
|    Número     |   nr    |
|     Data      |   dt    |
|    Valor      |   vl    |
|   Descrição   |   ds    |
|     Nome      |   nm    |
|     Tipo      |   tp    |
|   Quantidade  |   qt    |
|   Indicador   |   in    |

## Banco de Dados Postgresql
O banco de dados utilizado é o [Postgresql](https://www.postgresql.org/). Configure o arquivo `application.properties` com as propriedades do banco de dados:
```
  quarkus.datasource.db-kind = postgresql
  quarkus.datasource.jdbc.url=jdbc:postgresql://localhost:5432/postgres
  quarkus.datasource.username=postgres
  quarkus.datasource.password=xxxxxxxxx
  quarkus.datasource.jdbc.max-size=16
```
Para 
