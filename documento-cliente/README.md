# Projeto documento-server

Este projeto usa quarkus website: https://quarkus.io/ .

## Executando a aplicação em modo dev

use:
```shell script
./mvn compile quarkus:dev
```

## Empacotando e executando a aplicação

Empacote a aplicação usando:
```shell script
./mvn package
```
Este comando produz um jar `quarkus-run.jar` no diretório `target/quarkus-app/`.
Execute a aplicação com `java -jar target/quarkus-app/quarkus-run.jar`.

