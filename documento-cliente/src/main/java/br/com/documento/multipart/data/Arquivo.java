package br.com.documento.multipart.data;

import javax.activation.DataHandler;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlMimeType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Arquivo {

	private String nome;

	@XmlMimeType(MediaType.APPLICATION_OCTET_STREAM)
	private DataHandler arquivo;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public DataHandler getArquivo() {
		return arquivo;
	}

	public void setArquivo(DataHandler arquivo) {
		this.arquivo = arquivo;
	}

	@Override
	public String toString() {
		return "nome: " + this.nome + " arquivo: " + this.arquivo;
	}

}
