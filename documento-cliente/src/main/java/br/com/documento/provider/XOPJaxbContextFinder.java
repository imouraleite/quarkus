package br.com.documento.provider;

import javax.ws.rs.Produces;
import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;

import org.jboss.resteasy.plugins.providers.jaxb.JAXBContextFinder;
import org.jboss.resteasy.plugins.providers.jaxb.XmlJAXBContextFinder;

@Provider
@Produces({"application/xop+xml"})
public class XOPJaxbContextFinder extends XmlJAXBContextFinder implements ContextResolver<JAXBContextFinder> {

}
