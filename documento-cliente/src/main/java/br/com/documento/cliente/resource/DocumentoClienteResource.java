package br.com.documento.cliente.resource;

import java.io.File;
import java.nio.file.Files;

import javax.activation.DataHandler;
import javax.inject.Inject;
import javax.mail.util.ByteArrayDataSource;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;

import org.eclipse.microprofile.rest.client.inject.RestClient;

import br.com.documento.multipart.data.Arquivo;

@Path("/documento")
public class DocumentoClienteResource {

	@Inject
	@RestClient
	DocumentoClienteRestService clienteRestService;

	@POST
	@Path("/upload")
	public void sendFile() throws Exception {
		Arquivo arquivo = ArquivoFactory();
		clienteRestService.sendMultipartData(arquivo);
	}

	private Arquivo ArquivoFactory() throws Exception {
		Arquivo arquivo = new Arquivo();
		File file = new File("/home/imouraleite/workspace/ws_java/ws_infox/documentos/src/main/resources/banner.txt");
		arquivo.setArquivo(new DataHandler(
				new ByteArrayDataSource(Files.readAllBytes(file.toPath()), MediaType.APPLICATION_OCTET_STREAM)));
		arquivo.setNome("arquivo gerado automaticamente");
		return arquivo;
	}

}
