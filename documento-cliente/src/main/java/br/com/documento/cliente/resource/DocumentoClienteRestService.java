package br.com.documento.cliente.resource;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.jboss.resteasy.annotations.providers.multipart.XopWithMultipartRelated;
import org.jboss.resteasy.plugins.providers.multipart.MultipartConstants;

import br.com.documento.multipart.data.Arquivo;

@Path("/upload")
@RegisterRestClient
public interface DocumentoClienteRestService {

	@POST
	@Consumes(MultipartConstants.MULTIPART_RELATED)
	@Produces(MediaType.TEXT_PLAIN)
	String sendMultipartData(@XopWithMultipartRelated Arquivo arquivo);

}
